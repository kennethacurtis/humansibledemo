---
- name: add admin_user pass to credentials in tower
  tower_credential:
    name: admin_user
    organization: Default
    state: present
    kind: ssh
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"
    password: "{{ vault_admin_password }}"
    username: automateit


- name: add service principle credential to tower
  tower_credential:
    name: azure-sp-vscode
    organization: Default
    description: azure service principle for inventory sync
    state: present
    kind: azure_rm
    client: ee6dd8b9-fb5a-4ac2-80ff-0fece8bb0f32
    secret: "{{ vault_sp_secret }}"
    tenant: 56c62bbe-8598-4b85-9e51-1ca753fa50f2
    subscription: 09c13ad1-b084-4c6c-a211-68f4b4296074
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: create scm private file credential in tower
  tower_credential:
    name: humansibledemo
    organization: Default
    state: present
    kind: scm
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"
    ssh_key_data: "{{ vault_git_private_key }}"

- name: create vault credential in tower
  tower_credential:
    name: vault_password
    organization: Default
    state: present
    kind: vault
    vault_password: "{{ ansible_vault_password }}"
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: Add humansibledemo project
  tower_project:
    name: humansibledemo
    description: code to configure workshop
    organization: Default
    state: present
    scm_type: git
    scm_credential: humansibledemo
    scm_branch: dev
    scm_update_on_launch: yes
    scm_url: git@gitlab.com:kennethacurtis/humansibledemo.git
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: Add resource group inventory
  tower_inventory:
    name: humansibledemo001-vscode-rg
    description: vscode humansible resource group vms
    organization: Default
    state: present
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: Add tower humansibledemo001-vscode-rg inventory source
  tower_inventory_source:
    name: "{{ workshop_name }}-vscode-rg"
    description: vscode humansible resource group vms
    inventory: humansibledemo001-vscode-rg
    credential: azure-sp-vscode
    source: azure_rm
    update_on_launch: true
    overwrite: true
    state: present
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: get projects
  uri:
    url: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com/api/v2/projects/
    method: GET
    user: admin
    password: "{{ vault_admin_password }}"
    validate_certs: False
    status_code:
      - 200
      - 201
      - 202
    force_basic_auth: yes
  register: project_id

- name: find humansible project id
  set_fact:
    humansible_project_id: "{{ item.id }}"
  with_items: "{{ project_id.json.results }}"
  when: item.name == '{{ tower_project_name }}'

- debug:
    var: humansible_project_id

- name: sync project with tower
  uri:
    url: "https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com/api/v2/projects/{{ humansible_project_id}}/update/"
    method: POST
    user: admin
    password: "{{ vault_admin_password }}"
    validate_certs: False
    body: {}
    status_code:
      - 200
      - 201
      - 202
      - 204
    force_basic_auth: yes
    body_format: json

- name: wait for tower to sync code
  wait_for:
    timeout: 20

- name: create a job that pings hosts
  tower_job_template:
    name: Ping
    job_type: run
    inventory: humansibledemo001-vscode-rg
    project: humansibledemo
    playbook: ping.yml
    credential: admin_user
    vault_credential: vault_password
    state: present
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"

- name: Launch ping template (code and inventory will sync)
  tower_job_launch:
    job_template: Ping
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"
  register: job

- name: Wait for job max 120s
  tower_job_wait:
    job_id: "{{ job.id }}"
    timeout: 120
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"


- name: Add smart inventory to filter all student nodes
  tower_inventory:
    name: "{{ workshop_name }}-vscode-students"
    description: student nodes
    organization: Default
    kind: smart
    host_filter: search=student
    state: present
    tower_verify_ssl: no
    tower_host: https://{{ vm_name_prefix }}-master.{{ compute_region }}.cloudapp.azure.com
    tower_username: admin
    tower_password: "{{ vault_admin_password }}"